import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import api from '../api';
import { errorHandler } from './middlewares';

export default () : void => {
    const app = express();

    app.use(cors());
    app.use(bodyParser.json());
    app.use('/api', api);
    app.use(errorHandler)

    app.listen(8080, () => console.log(`API Server started and listening on port ${process.env.PORT} (${process.env.NODE_ENV})`));
};
