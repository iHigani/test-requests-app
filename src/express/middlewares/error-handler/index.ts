import { Request, Response, NextFunction } from 'express';

export default (err: Error, req: Request, res: Response, next: NextFunction) => {
    console.log(err);

    res.status(400).send(err.message);
};

