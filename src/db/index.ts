import { createConnection, Connection } from 'typeorm';
import RequestModel from '../api/request/request-model';
import AgentModel from '../api/agent/agent-model';

export default (): Promise<Connection> => {
    console.log('Connecting to the database')

    return createConnection({
        insecureAuth: true,
        type: 'mysql',
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [RequestModel, AgentModel],
        synchronize: true,
        logging: false
    })
}
