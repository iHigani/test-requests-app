import { Router } from 'express';
import requestRouter from './request';
import agentsRouter from './agent';

const router = Router();

router.use('/requests', requestRouter)
router.use('/agents', agentsRouter)

export default router;
