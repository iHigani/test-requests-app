import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

export enum Status {
    Pending = 'pending',
    Accepted = 'accepted',
    Closed = 'closed',
}

@Entity()
export default class Request {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    phone: string;

    @Column({
        type: 'enum',
        enum: Status,
        default: Status.Pending
    })
    status: Status;
}