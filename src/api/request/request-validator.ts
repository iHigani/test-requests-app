import joi from 'joi';
import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { Status } from './request-model';

export const createNewRequest = joi.object({
    name: joi.string().required(),
    phone: joi.string().required(),
    status: joi.valid(...Object.values(Status)).default(Status.Pending)
})

export interface ICreateNewRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: {
        name: string,
        phone: string,
        status: Status
    }
}

export const changeRequestStatus = joi.object({
    status: joi.valid(...Object.values(Status)).default(Status.Pending)
})

export interface IChangeRequest extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        requestID: string,
    },
    [ContainerTypes.Body]: {
        status: Status,
    }
}
