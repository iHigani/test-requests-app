import { Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import { getRepository } from 'typeorm';

import { ICreateNewRequest, IChangeRequest } from './request-validator';
import RequestModel, { Status } from './request-model';
import AgentModel from '../agent/agent-model';

export const createNewRequest = async (req: ValidatedRequest<ICreateNewRequest>, res: Response) => {
    const { body: requestDetails } = req;
    const agentRepository = getRepository(AgentModel);
    const requestRepository = getRepository(RequestModel);
    const request = requestRepository.create(requestDetails);

    await requestRepository.save(request);

    if (requestDetails.status === Status.Pending) {
        const agent = await agentRepository.findOne({ available: true })

        if (!agent) console.log('All agents are busy')
        else
            console.log(`
            Agent ID: ${agent.id}
            Agent Name: ${agent.name}
            Agent Phone: ${agent.phone}
            Request Name: ${request.name}
            Request Phone: ${request.phone}
            Request Status: ${request.status}
        `)
    }

    res.sendStatus(201);
}

export const changeRequestStatus = async (req: ValidatedRequest<IChangeRequest>, res: Response) => {
    const { body: { status }, params: { requestID } } = req;
    const requestRepository = getRepository(RequestModel);

    await requestRepository.update({ id: requestID }, { status });

    res.sendStatus(200);
}
