import { AsyncRouter as router } from 'express-async-router';
import { requestValidator } from '../../services';
import * as controller from './request-controller';
import * as requestSchemas from './request-validator';

const requestsRouter = router();

requestsRouter.post(
    '/',
    requestValidator.body(requestSchemas.createNewRequest),
    controller.createNewRequest
)

requestsRouter.put(
    '/:requestID/status',
    requestValidator.body(requestSchemas.changeRequestStatus),
    controller.changeRequestStatus
)

export default requestsRouter;