import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';


@Entity()
export default class Agent {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    phone: string;

    @Column('boolean')
    available: boolean;
}