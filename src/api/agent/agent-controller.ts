import { Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import { getRepository } from 'typeorm';

import { IChangeAvailability } from './agent-validator';
import AgentModel from './agent-model';

export const changeAvailability = async (req: ValidatedRequest<IChangeAvailability>, res: Response) => {
    const { body: { agentID, available } } = req;
    const agentRepository = getRepository(AgentModel);

    await agentRepository.update({ id: agentID }, { available });

    res.sendStatus(200);
}