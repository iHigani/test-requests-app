import { AsyncRouter as router } from 'express-async-router';
import { requestValidator } from '../../services';
import * as controller from './agent-controller';
import * as agentSchemas from './agent-validator';

const agentsRouter = router();

agentsRouter.put(
    '/availability',
    requestValidator.body(agentSchemas.changeAvailability),
    controller.changeAvailability
)

export default agentsRouter;