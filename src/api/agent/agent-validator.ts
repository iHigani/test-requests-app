import joi from 'joi';
import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'

export const changeAvailability = joi.object({
    agentID: joi.string().required(),
    available: joi.boolean().required(),
})

export interface IChangeAvailability extends ValidatedRequestSchema {
    [ContainerTypes.Body]: {
        agentID: string,
        available: boolean,
    },
}