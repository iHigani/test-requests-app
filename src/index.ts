import startServer from './express';
import startDatabase from './db';
import dotenv from 'dotenv';

dotenv.config();

startDatabase()
    .then(() => startServer())
    .catch(() => console.log('Error connecting to database'));

